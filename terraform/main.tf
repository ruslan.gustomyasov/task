module "task" {
  source = "./modules/task"
  network_link = google_compute_network.task.self_link
}