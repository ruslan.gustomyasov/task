variable "network_link" {
  
}

module "gke" {
  source            = "terraform-google-modules/kubernetes-engine/google"
  project_id        = "decent-tracer-255319"
  name              = "task-prod"
  regional          = false
  region            = "europe-west1"
  zones             = ["europe-west1-b"]
  network           = "task"
  subnetwork        = "task-prod"
  ip_range_pods     = "task-prod-pods"
  ip_range_services = "task-prod-services"
  service_account   = "create"
  monitoring_service = "none"
  logging_service   = "none"
  http_load_balancing	= false
  grant_registry_access = true

 node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "e2-small"
      min_count          = 3
      max_count          = 3
      disk_size_gb       = 10
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = true
      initial_node_count = 3
    },
  ]
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = var.network_link
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = var.network_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}


resource "google_sql_database_instance" "postgres" {
  name             = "postgres"
  database_version = "POSTGRES_11"
  region           = "europe-west1"
  depends_on = [google_service_networking_connection.private_vpc_connection]
  settings {
    tier = "db-f1-micro"
    disk_size = "10"
    disk_type = "PD_SSD"
    ip_configuration {
      ipv4_enabled = false
      private_network = var.network_link
    }
    backup_configuration {
      enabled=true
    }
  }
}

resource "google_sql_database" "api" {
  name     = "api"
  instance = google_sql_database_instance.postgres.name
}

resource "google_sql_user" "api" {
  name     = "api"
  instance = google_sql_database_instance.postgres.name
  password = "password"
}

resource "google_service_account" "registry" {
  account_id   = "registry"
  display_name = "registry"
}

resource "google_project_iam_binding" "registry" {
  role = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.registry.email}",
  ]
}

# resource "google_compute_global_address" "ingress" {
#   name = "ingress"
#   address = "34.107.184.93"
# }

resource "google_dns_managed_zone" "rusik" {
  name     = "rusik"
  dns_name = "rusik.dev."
}

resource "google_dns_record_set" "rusik" {
  name         = google_dns_managed_zone.rusik.dns_name
  managed_zone = google_dns_managed_zone.rusik.name
  type         = "A"
  ttl          = 300

  rrdatas = ["116.203.222.149"]
}

resource "google_dns_record_set" "web" {
  name         = "web.rusik.dev."
  managed_zone = google_dns_managed_zone.rusik.name
  type         = "A"
  ttl          = 300

  rrdatas = ["35.241.195.101"]
}

