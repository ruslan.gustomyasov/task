# gcloud auth application-default login
# https://console.cloud.google.com/apis/credentials/serviceaccountkey

terraform {
  backend "gcs" {
    bucket  = "terraform-rusik-dev"
    prefix  = "terraform/state"
  }
}

data "terraform_remote_state" "foo" {
  backend = "gcs"
  config = {
    bucket  = "terraform-rusik-dev"
    prefix  = "terraform/state"
  }
}

provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "decent-tracer-255319"
  region      = "europe-west1"
}

resource "google_compute_network" "task" {
  name                    = "task"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "task-prod" {
  name          = "task-prod"
  ip_cidr_range = "10.1.0.0/16"
  secondary_ip_range {
    range_name    = "task-prod-pods"
    ip_cidr_range = "10.2.0.0/16"
  }
  secondary_ip_range {
    range_name    = "task-prod-services"
    ip_cidr_range = "10.3.0.0/16"
  }
  network = google_compute_network.task.self_link
  region  = "europe-west1"
}
